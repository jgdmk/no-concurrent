package org.yun.biz.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liyunfeng31
 */
@Data
@AllArgsConstructor
public class TinyRedPack implements Serializable {

    private String tId;

    private String amount;

}
