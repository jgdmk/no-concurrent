package org.yun.biz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.yun.biz.model.DrawRecord;


/**
 * @ProjectName: no-concurrent
 * @ClassName: DrawRecordRepository
 * @Description: DrawRecordDao
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:42
 */
public interface DrawRecordRepository extends JpaRepository<DrawRecord, Long> {

}