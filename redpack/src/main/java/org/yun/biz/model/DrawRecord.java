package org.yun.biz.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.yun.util.IDUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author liyunfeng31
 */
@Data
@Table(name = "draw_record")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DrawRecord implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "custom-id")
    @GenericGenerator(name = "custom-id", strategy = "org.yun.util.IDUtil")
    @Column(name = "tinyrd_id")
    private Long tinyRdId;

    @Column(name = "red_pack_id")
    private Long redPackId;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "draw_state")
    private Integer drawState;

    @Column(name = "statements_id")
    private Long statementsId;

    @Column(name = "c_t")
    private Date ct;



    public static DrawRecord initRecord(Long redPackId, Integer amount){
        System.out.println(amount);
        return DrawRecord.builder()
                .tinyRdId(IDUtil.id())
                .userId(0L)
                .userName("")
                .avatar("")
                .redPackId(redPackId)
                .amount(amount)
                .drawState(0)
                .statementsId(0L)
                .ct(new Date()).build();
    }
}
