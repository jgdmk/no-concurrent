package org.yun.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.yun.config.RedisConfig;

/**
 * @ProjectName: no-concurrent
 * @ClassName: Task
 * @Description: TODO(一句话描述该类的功能)
 * @Author: liyunfeng31
 * @Date: 2020/10/6 22:40
 */
@Component
public class Task {


    private static final Logger log = LoggerFactory.getLogger(RedisConfig.class);

    @Scheduled(cron = "0/5 * * * * *")
    public void scheduled(){

        log.info("=====>>>>>使用cron  {}",System.currentTimeMillis());
    }
}
