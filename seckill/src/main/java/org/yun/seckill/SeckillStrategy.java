package org.yun.seckill;

/**
 * @ProjectName: no-concurrent
 * @ClassName: Strategy
 * @Description: 秒杀策略接口
 * @Author: liyunfeng31
 * @Date: 2020/10/4 23:19
 */
public interface SeckillStrategy {

  /**
   * @param userId 用户ID
   * @param skuId SKU-ID
   * @param activityId 活动ID
   * @param decrement 减量
   * @param ip ip
   * @return 是否成功 1-OK  0-FAIL
   */
  int secKill(Long userId, Long skuId, Long activityId, Integer decrement, String ip);
}
