package org.yun.biz.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author liyunfeng31
 */
@Data
@Entity
@Table(name = "event_log")
public class EventLog implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "biz_id")
    private Long bizId;

    @Column(name = "biz_code")
    private Integer bizCode;

    @Column(name = "content")
    private String content;

    @Column(name = "owner")
    private String owner;

    @Column(name = "c_t")
    private Date ct;


    public EventLog(){
    }

    public EventLog(Long bizId, Integer bizCode, String content, String owner) {
        this.bizId = bizId;
        this.bizCode = bizCode;
        this.content = content;
        this.owner = owner;
    }
}
