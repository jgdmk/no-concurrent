package org.yun.biz.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Version;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @ProjectName: no-concurrent
 * @ClassName: SkuStock
 * @Description: TODO(一句话描述该类的功能)
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:09
 */

@Data
@Entity
@Table(name = "seckill_stock")
public class Stock implements Serializable {

    @Id
    @Column(name = "sku_id")
    private Long skuId;

    @Column(name = "total_stock")
    private Integer totalStock;

    @Column(name = "valid_stock")
    private Integer validStock;

}
