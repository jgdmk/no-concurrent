package org.yun;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.yun.biz.dao.StockRepository;
import org.yun.biz.model.Stock;
import org.yun.config.IConfig;
import org.yun.constants.RedisConstant;
import org.yun.seckill.NormalStrategy;
import org.yun.seckill.SimpleStrategy;
import org.yun.seckill.UltimateStrategy;
import org.yun.util.IDUtil;
import org.yun.util.RedisUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
@SpringBootTest
class SecKillTests {


    @Resource
    private RedisUtil redisUtil;

    @Resource
    private SimpleStrategy simpleStrategy;

    @Resource
    private NormalStrategy normalStrategy;

    @Resource
    private UltimateStrategy ultimateStrategy;

    @Resource
    private StockRepository stockDao;

    @Resource
    private IConfig config;



    public static final Long userId = 123L;

    public static final Long skuId = 321L;

    public static final Long activityId = 999L;

    public static final int num = 1;

    public static final String ip = "0.0.0.0";


    @Test
    void addActivity() {
        Map<String, Object> actMap = new HashMap<>();
        actMap.put("state","1");
        actMap.put("st","1602259200");
        actMap.put("et","1665331200");
        actMap.put("appointment","0");
        String key = RedisConstant.ACTIVITY_SKU + activityId + "_" + skuId;
        boolean l = redisUtil.hmset(key, actMap);
        System.out.println(l);
    }

    @Test
    void syncStockToCache() {
        Stock stock = stockDao.findAll().get(0);
        String skuAct = skuId +":"+ activityId;
        String key = RedisConstant.STOCK + skuAct;
        System.out.println(JSON.toJSONString(stock));
        redisUtil.set(key,stock.getValidStock());
    }


    @Test
    void simpleStrategyTest() {
        for (int i = 0; i < 12; i++) {
            int result = simpleStrategy.secKill(userId, skuId, activityId, num, ip);
            System.out.println(result);
        }
    }


    @Test
    void normalStrategyTest() {
        for (int i = 0; i < 12; i++) {
            int result = normalStrategy.secKill(userId+i, skuId, activityId, num, ip);
            System.out.println(result);
        }
    }


    @Test
    void UltimateStrategyTest() {
        for (int i = 0; i < 12; i++) {
            int result = ultimateStrategy.secKill(userId+i, skuId, activityId, num, ip);
            System.out.println(result);
        }
        int result = ultimateStrategy.secKill(userId, skuId, activityId, num, ip);
        System.out.println(result);
    }






    public static int clientTotal = 2000;

    public static int threadTotal = 20;

    public static int count = 0;


    @Test
    void test() throws InterruptedException {
        //定义线程池
        AtomicInteger success = new AtomicInteger();
        AtomicInteger all = new AtomicInteger();

        ExecutorService executorService = Executors.newCachedThreadPool();
        //定义信号量,给出允许并发的线程数目
        final Semaphore semaphore = new Semaphore(threadTotal);
        //统计计数结果
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        //将请求放入线程池
        for (int i = 0; i < clientTotal ; i++) {
            executorService.execute(() -> {
                try {
                    //信号量的获取
                    semaphore.acquire();
                    int result = ultimateStrategy.secKill(IDUtil.id(), skuId, activityId, num, ip);
                    all.getAndIncrement();
                    if(result == 1){
                        success.getAndIncrement();
                    }
                  //  System.out.println(all.get());
                    //释放
                    semaphore.release();
                } catch (Exception e) {
                    log.error("exception", e);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();        //关闭线程池
        executorService.shutdown();
        log.info("all:{}", all.get());
        log.info("count:{}", success.get());
    }




}
